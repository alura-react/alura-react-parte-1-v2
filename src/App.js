import React, {Component, Fragment} from "react";
import "materialize-css/dist/css/materialize.min.css";
import "./App.css";
import Header from "./Header";
import Tabela from "./Tabela";
import Formulario from "./Formulario";

export default class App extends Component {
    state = {
        autores: [
            {
                nome: "Paulo",
                livro: "React",
                preco: "45"
            }, {
                nome: "Daniel",
                livro: "Java",
                preco: "99"
            }, {
                nome: "Marcos",
                livro: "Design",
                preco: "150"
            }, {
                nome: "Bruno",
                livro: "DevOps",
                preco: "100"
            }, {
                nome: "Outro autor",
                livro: "Outro livro",
                preco: "25"
            }
        ]
    };

    removeAutor = index => {
        const {autores} = this.state;

        this.setState({
            autores: autores.filter(
                (autor, posAtual) => {
                    return posAtual !== index;
                }
            )
        });
    };

    escutadorDeSubmit = autor => {
        this.setState({
            autores: [
                ...this.state.autores,
                autor
            ]
        });
    };

    render() {
        return (
            <Fragment>
                <Header/>
                <div className="container mb-10">
                    <h1>Casa do código</h1>
                    <Tabela autores={
                            this.state.autores
                        }
                        removeAutor={
                            this.removeAutor
                        }/>
                    <Formulario escutadorDeSubmit={
                        this.escutadorDeSubmit
                    }/>
                </div>
            </Fragment>
        );
    }
}
